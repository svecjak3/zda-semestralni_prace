# ZDA-Semestrální_práce
V semestrální práci se zabívám tím, zda existuje nějaká spojitost mezi inteligencí a výškou člověka ve světě.
A také zda má inteligence vliv na hodnocení škol případně, zda existuje zde nějaká lineární závislost.

# Motivace
Téma jako takové mi přijde zajímavé, lidé běžně hodnotě IQ přiřazují příliš velkou váhu. <br>
Kdysi jsem narazil na tvrzení, že výška lidí je pozitivní predispozice (rys). <br>
A že obvykle tam kde je větší koncentrace vyšších lidí tak je průměrně i více inteligentních lidí.<br>

Přišlo mi zajímavé se tím zabývat a v průběhu sběru dat, jsem si říkal zda existuje i spojitost mezi hodnocením škol v jednotlivých státech<br>
a průměrnou inteligencí daného státu.<br>

# Použité data
https://worldpopulationreview.com/country-rankings/average-height-by-country <br>
https://worldpopulationreview.com/country-rankings/average-iq-by-country <br>
https://worldpopulationreview.com/country-rankings/education-rankings-by-country <br>

Data ohledně IQ byla ověřena i z jiných zdrojů

Finální použitá tabluka je přiložena ve formátu csv a xlsx <br>
XLSX: <a href="https://gitlab.fel.cvut.cz/svecjak3/zda-semestralni_prace/-/blob/main/semestralka.xlsx"> tabluka dat v xlsx </a><br>
CSV: <a href="https://gitlab.fel.cvut.cz/svecjak3/zda-semestralni_prace/-/blob/main/semestralkadata.csv"> tabluka dat v csv </a>

# Postup
Nejprve jsem si našel světová data, pro jednotlivé státy viz horní příloha.
Dalším krokem byla vizualizace, co z aktuálních dat vyčtu. 
    
    V tomto kroku jsem narazil na problém. Číselná data jsou v textovém typu.
    Zároveň tabulky obsahovali příliš dat, které jsou nepodstatné v rámci této analýzy
    Proto jsem se rozhodl první o textovou úpravu tabulek (Nahrazení znaků jako je oddělovač)
    A následně nahrazení tečky u desetinných čísel čárkou.
    Tento soubor nebyl použitelný v power BI proto přišla na řadu trasformace dat přímo v power Bi
    Pomocí mergování tabulek se mi podařilo vytvořit jednotnou tabulku.
    To vedlo ke snížení pamťové nročnosti a k možnosti vizualizovat data, která potřebuji.

Třetí krok bylo vytvoření python souboru pro práci z daty.
    
    Zde vznikl nový problém se čtením dat ze souboru csv. 
    Proto bylo nutné provést další úpravy (změna znaků, nahrazení prázdných polí za null).
    Dalším krokem bylo zobrazení grafu.
    Poté provedení metody čtverců
    Následně jsem se rozhodl použít knihovnu na lineární regresi a srovnat ji s metodou OSL
    V posledním kroku jsem provedl pokus o kauzalitu mezi daty Iq a výška člověka.
    
    Pozn.V pythonu pracuji především s průměrnou výškou muže.
    Výšky jsou grafově podobné jen posunuté

Následně jsem vyvodil závěry
# Grafy
Hodnoty v následujících grafech jsou hodnoty naměřené v jednotlivých státech (každá modra tečka reprezentuje stát).<br>
V grafech vidíme implementovanou metodu čtverců (OSL), kde je možné si všimnout jistého trendu.<br>
Ten je znázorněn červenou čarou. <br>
Nad grafem jsou uvedeny hodnoty: Sklon, Posun a korelační koeficient. <br>
Sklon udává rychlost změny závislé proměné k poměru nezávislé proměné. <br>
Posun nám udává hodnotu Y (Výška/Rank školy) kdy se červená osa protne s osou X (Iq).<br>
korelační koeficient je v rozsahu (-1,1) a čím více je k hodnotě 1 nebo -1 tím větší lin. závislost mezi proměnnými je.<br>
Pokud by hodnota byla 0 jednalo by se o absolutně lin. nezavislé data.<br>

![graf](./images_for_readme/OSL_iq-vy.PNG)
![graf1](./images_for_readme/OSL_iq_rank.PNG)

<br>
V následujících grafech jsme implementovali lin. regresi za použití knihovny sklearn.linear_model<br>
kdy jsem porovnal výsledky z metody čtverců kteoru jsem vytvořil na základě matematických vzorců.<br>
V případě školy se jedná o totožná data a výsledky jsou téměř totožné. Jedná se pouze o odchylku způsobenou způsobem zaokrouhlování.<br>
V případě výšky byla data v druhém případě rozdělana na trénovací a testovací. Testovací však nakonec nebyla použita.<br>
Data bylo nutné vždy sjednotit a to odstraněním hodnot, které chybí v jednou nebo druhé datasetu. <br>

![graf2](./images_for_readme/lnr_iq_vy.PNG)
![graf3](./images_for_readme/lnr_iq_rank.PNG)
<br>
Kauzalita se nepovedla a jednalo se spíše o pokus dostat alespoň nějak rozumný výsledek.<br>
Z důvodu, že se jedná o poměrně nesmysl nebudu jej zde promítat. K nahlednutí je ovšem v přiloženém souboru.<br>

Tato část proběhla pomocí výpočtů v pythonu. Kompletní kód včetně poznámek je zde:<a href="https://gitlab.fel.cvut.cz/svecjak3/zda-semestralni_prace/-/blob/main/semestralkadata.csv"> kód pythonu </a>

Nyní se podíváme na grafy vytvořené v programu Power BI.<br>
Obrázek je rozdělen na 4 kvartály.<br>
První znázorňuje korelaci mezi výškou můžu a iq.<br>
Druhý znázorňuje korelaci mezi výškou žen a iq.<br>
Třetí znázorňuje velikost iq a výšku obou pohlaví v jednotlivých regionech.<br>
Čtvrtý znázorňuje rank školství (rank = menší lepší) a výšku obou pohlaví v jednotlivých regionech.<br>
![grafPowerBi](./images_for_readme/poweBi.PNG)

# Závěr 1. (Co vyplývá z analýzy)
Pokud vezme zdravého člověka, který nemá žádnou genetickou vadu vzrůstu nebo mentálního zdraví.<br>
Pak na základě výstupů nemůžeme zaručeně říci, že neexistuje žádná spojitost mezi výškou a inteligencí, či kognitivními schopnosti jedince.<br>
Korelační koeficient se pohybuje mezi hodnotami(0.55-0.60).<br>
Na druhou stranu, existuje příliš mnoho faktorů jako je genetika, strava, okolí, vzdělání, životní události a další. <br>
Které mohou ovlivňovat jak konečnou výšku jedince tak jeho inteligenci.<br>
Hodnota IQ vyjadřuje pouze relativní úroveň inteligence a je odvozena z výsledků standardizovaných psychometrických testů.<br>
Existuje mnoho různých druhů inteligence, které nelze zcela měřit pouze pomocí jednoho čísla.<br>
IQ testy mohou poskytnout určitý odhad schopností jednotlivce, ale nemají za cíl posoudit veškeré aspekty inteligence a potenciálu člověka.<br>

Dodatečná analýza IQ a úroveň škol v daném státě kdy korelace vyšla zaokrouhleně 0.6.<br>
Nám říká, že čím lepší školství tím lepší vysledky v IQ testech. <br>
Nebo čím lepší výsledky v IQ testech tím lepší výsledky má škola.<br>

Odpověď na otázku zda existuje spojitost mezi výškou a inteligencí zdravého člověka (bez genetických vad).<br>
Je ano existuje, ale je poměrně zanedbatelná v poměru s ostatními faktory.

# Závěr 2. (Co mi práce dala)
Procvičil jsem si práci s power Bi a programování v pythonu.<br>
Našel jsem zajimavou stránku pro hledání dat z jednotlivých zemí.<br>
Naučil jsem se používat metodu čtverců lépe jsem ji porozuměl.<br>
Zpětně si myslím, že jsem lépe porozuměl kauzalitě.<br>
V průběhu analýzy jsem měl problém s rozdělování na závislé a nezávislé proměnné. <br>

Ve svých dalších analýzách budu zřejmě lépe vědět, jaké data budu předem potřebovat z jakými proměnnými budu pracovat.<br>
Jak effektivně je dostat do stavu, kdy budou použitelné.<br>
A v konečném důseldku i lépe aplikovat vzorce.<br>

Práce mě bavila, občas jsem se zasekl na někjakém problému, udělal spoustu chyb. <br>
Vzhledem k časovému presu, který ke konci semestru vzniká jsem se svou prací spokojen a považuji ji za velmi přínosnou.<br>




